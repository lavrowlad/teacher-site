
/** articles indexes **/
db.getCollection("articles").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** authors indexes **/
db.getCollection("authors").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** files indexes **/
db.getCollection("files").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** pages indexes **/
db.getCollection("pages").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** articles records **/
db.getCollection("articles").insert({
  "_id": ObjectId("5c19c447daaf17e00b000029"),
  "author": ObjectId("5c19c510daaf17d414000032"),
  "created-date": {
    "day": "28",
    "month": "ноября",
    "year": "2018",
    "time": "5:12"
  },
  "published-date": {
    "day": "28",
    "month": "ноября",
    "year": "2018",
    "time": "5:14"
  },
  "title": "Педагогічне кредо",
  "text": "Мета навчання дитини полягає в тому, щоб зробити її здатною розвиватися далі без допомоги вчителя Е. Хаббар",
  "statistics": {
    "views": NumberInt(265),
    "likes": NumberInt(112)
  },
  "tags": [
    "tag a",
    "tag b",
    "tag c"
  ]
});
db.getCollection("articles").insert({
  "_id": ObjectId("5c19c447daaf17e00b00002a"),
  "author": ObjectId("5c19c510daaf17d414000032"),
  "created-date": {
    "day": "28",
    "month": "ноября",
    "year": "2018",
    "time": "5:12"
  },
  "published-date": {
    "d": "17",
    "m": "ноября",
    "y": "2018",
    "t": "5:19"
  },
  "title": "Загальні відомості",
  "text": " Ракша Світлана Олександрівна\r\n      Дата народження: 28 травня 1965 р.\r\n      Освіта: вища Закінчила у 1987 році Харківський державний \r\n      педагогічний інститут ім. Г.С. Сковороди за спеціальністю \r\n      «Педагогіка та методика початкового навчання»\r\n      Професія: вчитель початкових класів\r\n      Місце роботи: Чугуївська ЗОШ I – III ступенів № 2 \r\n      Стаж роботи: 27 років\r\n      Категорія: перша\r\n      Курси підвищення кваліфікації пройшла в 2012 році.\r\n      Керівник шкільного методичного об’єднання вчителів початкових \r\n      класів з 2011 р.\r\n      ",
  "statistics": {
    "views": NumberInt(305),
    "likes": NumberInt(225)
  },
  "tags": [
    "tag a"
  ]
});
db.getCollection("articles").insert({
  "_id": ObjectId("5c19c447daaf17e00b00002b"),
  "author": ObjectId("5c19c510daaf17d414000032"),
  "created-date": {
    "day": "28",
    "month": "ноября",
    "year": "2018",
    "time": "5:12"
  },
  "published-date": {
    "d": "28",
    "m": "сентября",
    "y": "2018",
    "t": "5:27"
  },
  "title": "Основні педагогічні правила",
  "text": "Дитині, як і дорослому потрібна свобода думок.\r\n      Розвивай в учня допитливість, творчу фантазію.\r\n      Допомагай знайти істину в ході дискусії.\r\n      Підтримай і допоможи тому, хто цього потребує.\r\n      Умій зацікавити.\r\n      Люби дітей і вони віддячать тобі тим же.\r\n    ",
  "statistics": {
    "views": NumberInt(401),
    "likes": NumberInt(297)
  },
  "tags": [
    "tag b",
    "tag c"
  ]
});

/** authors records **/
db.getCollection("authors").insert({
  "_id": ObjectId("5c19c510daaf17d414000032"),
  "fname": "Vlad",
  "lname": "Lavrov"
});

/** files records **/
db.getCollection("files").insert({
  "_id": ObjectId("5c19c909daaf17181c000029"),
  "title": "Презентація досвіду",
  "link": "./files/Презентація досвіду.ppt"
});
db.getCollection("files").insert({
  "_id": ObjectId("5c19c909daaf17181c00002a"),
  "title": "Портфоліо",
  "link": "./files/Портфоліо.ppt"
});
db.getCollection("files").insert({
  "_id": ObjectId("5c19c909daaf17181c00002b"),
  "title": "Методична робота",
  "link": "./files/Методична робода.ppt"
});
db.getCollection("files").insert({
  "_id": ObjectId("5c19c909daaf17181c00002c"),
  "title": "Засідання вчителів",
  "link": "./files/ШМО/Засідання вчителів початкових класів №1.pptx"
});
db.getCollection("files").insert({
  "_id": ObjectId("5c19c909daaf17181c00002d"),
  "title": "Документація Планета мрій",
  "link": "./files/Планета мрій/документация лагерь.docx"
});
db.getCollection("files").insert({
  "_id": ObjectId("5c19c909daaf17181c00002e"),
  "title": "Список працівників Планета мрій",
  "link": "./files/Планета мрій/Список працівників.docx"
});
db.getCollection("files").insert({
  "_id": ObjectId("5c19c909daaf17181c00002f"),
  "title": "Thumbs.db",
  "link": "./files/Планета мрій/Приказы/Лагерь на конкурс/Thumbs.db"
});
db.getCollection("files").insert({
  "_id": ObjectId("5c19c909daaf17181c000030"),
  "title": "Гімн та закони",
  "link": "./files/Планета мрій/Приказы/Лагерь на конкурс/гімн закони.jpg"
});

/** pages records **/
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d414000029"),
  "valid": true,
  "link": "index.php",
  "title": "Головна"
});
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d41400002a"),
  "valid": true,
  "link": "portfolio.php",
  "title": "Портфоліо"
});
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d41400002b"),
  "valid": true,
  "link": "archives.php",
  "title": "Архів"
});
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d41400002c"),
  "valid": false,
  "link": "metodychna-robota.php",
  "title": "Методична робота"
});
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d41400002d"),
  "valid": false,
  "link": "vihovna-robota.php",
  "title": "Виховна робота"
});
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d41400002e"),
  "valid": false,
  "link": "planeta-mriy.php",
  "title": "\"Планета мрій\""
});
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d41400002f"),
  "valid": false,
  "link": "shmo.php",
  "title": "ШМО"
});
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d414000030"),
  "valid": false,
  "link": "grupa-podovjenogo-dnya.php",
  "title": "Група подовженого дня"
});
db.getCollection("pages").insert({
  "_id": ObjectId("5c19bb2edaaf17d414000031"),
  "valid": false,
  "link": "robota-z-batkamy.php",
  "title": "Робота з батькамі"
});

/** system.indexes records **/
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "teacher_site.pages"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "teacher_site.articles"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "teacher_site.authors"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "teacher_site.files"
});
