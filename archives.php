<?php require '/components/header.php' ?>

<link rel="stylesheet" href="./css/archives.css">

<div class="content">
  <div class="files">
    <?php
      try {
        $connect = new Mongo();
        $db = $connect->teacher_site;
        $files = $db->files;
  
        $cursor = $files->find();
        foreach ($cursor as $file) {
          $info = new SplFileInfo($file['link']);
          $img_ext = $info->getExtension();

          echo "<div class='item'>";
          echo "<img src='./images/".$img_ext.".png' />";
          echo "<a href='".$file['link']."'>".$file['title']."</a>";
          echo "</div>";
        };
        
        $connect->close();
      } catch (MongoConnectionException $e) {
        die('Error connection to MongoDB server');
      } catch (MongoException $e) {
        die('Error: ' . $e->getMessage());
      }
    ?>
  </div>
</div>

<?php require '/components/footer.php' ?>

