<?php require '/components/header.php' ?>

<link rel="stylesheet" href="./css/index.css">

<div class="content">
  <?php
    try {
      $connect = new Mongo();
      $db = $connect->teacher_site;
      $pages = $db->pages->find();

      require '/components/pages.php';

      $connect->close();
    } catch (MongoConnectionException $e) {
      die('Error connection to MongoDB server');
    } catch (MongoException $e) {
      die('Error: ' . $e->getMessage());
    }
  ?>
</div>

<?php require "./components/sitebar.php" ?>
<?php require '/components/footer.php' ?>
