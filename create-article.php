<?php require '/components/header.php' ?>

<link rel="stylesheet" href="./css/create-article.css">

<div class="content">
  <form id="create-article">
    <label for="title">Заголовок</label><br>
    <input type="text" id="title" name="title">

    <br>

    <label for="text">Текст статті</label><br>
    <textarea name="text" id="text" cols="30" rows="10"></textarea>

    <br>

    <div class="tags">
      <div class="header">Теги</div>
      <?php
        try {
          $connect = new Mongo();
          $db = $connect->teacher_site;
          $tags = $db->tags;
    
          $cursor = $tags->find();
          foreach ($cursor as $tag) {
            ?>
            <label class="item" for="tag-<?php echo $tag['number']; ?>">
              <input type="checkbox" name="tags" id="tag-<?php echo $tag['number']; ?>" >
              <?php
                echo $tag['name'];
              ?>
            </label>
            <?php
          };
          
          $connect->close();
        } catch (MongoConnectionException $e) {
          die('Error connection to MongoDB server');
        } catch (MongoException $e) {
          die('Error: ' . $e->getMessage());
        }
      ?>
      <div class="add-new-tag">Додати новий</div>
    </div>
    <button id="create-article-btn">Создать</button>
  </form>

  <div id="result-creating"></div>
</div>

<?php require '/components/footer.php' ?>
