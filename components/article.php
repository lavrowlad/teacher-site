<?php
  

  $title      = $article['title'];
  $text       = $article['text'];
  $publiched  = $article['published-date'];
  $views      = $article['statistics']['views'];
  $likes      = $article['statistics']['likes'];
  $tags       = $article['tags'];
?>

<article>
  <div class="header">
    <div class="title"><?php echo $title ?></div>
    <div class="published">
    <div class="day"><?php echo $publiched['d'] ?></div>
    <div class="month"><?php echo $publiched['m'] ?></div>
  </div>
  </div>
  <div class="tags">
      <?php foreach ($tags as $i => $value) {
        $tag = $tags[$i];
        echo "<div class='tag'><div class='before'></div>".$tag."</div>";
      } ?>
    </div>
  <div class="text">
    <?php echo $text ?>
  </div>
  <div class="footer">
    <div class="views"><?php echo $views ?></div>
    <div class="likes"><?php echo $likes ?></div>
  </div>
</article>