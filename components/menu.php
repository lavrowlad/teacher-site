<div class="menu">
  <?php
    try {
      $connect = new Mongo();
      $db = $connect->teacher_site;
      $pages = $db->pages;
    
      $cursor = $pages->find(array('valid' => true));
    
      foreach ($cursor as $page) {
        echo "<a href=".$page['link']." class='item'>".$page['title']."</a>";
      }
    
      $connect->close();
    } catch (MongoConnectionException $e) {
      die('Error connection to MongoDB server');
    } catch (MongoException $e) {
      die('Error: ' . $e->getMessage());
    }
  ?>
</div>