<link rel="stylesheet" href="./css/admin.css">

<div class="pages">
  <div class="header">
    <div class="check"></div>
    <div class="title">Заголовок</div>
    <div class="link">Ссылка</div>
    <div class="edit"></div>
    <div class="delete"></div>
  </div>
  <div class="items">

    <!-------------------------->
    <?php foreach ($pages as $page) { ?>
      <div class="item">
        <div class="check"><img src="./images/icons/<?php if($page['valid']) {echo 'check';} else {echo 'uncheck';}; ?>.png"></div>
        <div class="title"><?php echo $page['title']; ?></div>
        <div class="link"><?php echo $page['link']; ?></div>
        <div class="edit"><img src="./images/icons/edit.png" alt=""></div>
        <div class="delete"><img src="./images/icons/delete.png" alt=""></div>
      </div>
    <?php } ?>
    <!-------------------------->

  </div>
  <div class="footer">
    <div class="check"><input type="checkbox" checked></div>
    <div class="title"><input type="text"></div>
    <div class="link"><input type="text"></div>
    <div class="add"></div>
  </div>
</div>