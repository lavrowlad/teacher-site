<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Teacher</title>  

  <link rel=icon href='./images/favicon.ico' sizes="16x16" type="image.ico">
  <link rel="stylesheet" href="css/style.css" />
</head>
<body>

  <header>
      <img src="./images/backgound.jpg" alt="">
      <div class="text">
          <div class="title">Ракша Світлана Олександрівна</div>
          <div class="subtitle">Вчитель початкових класів</div>
      </div>
      <div class="photo">
        <img src="./images/foto.jpg" alt="">
      </div>
    </header>

  <div class="main">
    <div class="search">
      <?php require '/components/search-panel.php' ?>
    </div>

    <section>

      <nav>
        <?php require '/components/menu.php' ?>
      </nav>

      <section>


